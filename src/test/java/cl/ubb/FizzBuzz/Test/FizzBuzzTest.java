package cl.ubb.FizzBuzz.Test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.FizzBuzz.FizzBuzz;

public class FizzBuzzTest {
		
		@Test
		public void RecibeUnoDevuelveUno() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(1);
			//assert
			assertEquals("1",retorno);
			
		}
		
		@Test
		public void RecibeDosDevuelveDos() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(2);
			//assert
			assertEquals("2",retorno);
			
		}
		
		@Test
		public void RecibeTresDevuelveFizz() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(3);
			//assert
			assertEquals("Fizz",retorno);
			
		}
		
		@Test
		public void RecibeCuatroDevuelveCuatro() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(4);
			//assert
			assertEquals("4",retorno);
			
		}
		
		@Test
		public void RecibeCincoDevuelveBuzz() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(5);
			//assert
			assertEquals("Buzz",retorno);
			
		}
		
		@Test
		public void RecibeQuinceDevuelveFizzBuzz() {
			String retorno;
			//arrange
			FizzBuzz fizzbuzz = new FizzBuzz();
			//act
			retorno = fizzbuzz.game(15);
			//assert
			assertEquals("FizzBuzz",retorno);
			
		}


}
